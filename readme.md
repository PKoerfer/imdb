# Project Overview

![image info](./app/src/main/res/mipmap-xxxhdpi/ic_launcher_round.png "App Launcher Icon")

Following the overall setup of the project is described, containing used permissions, dependencies etc.


## Architecture

Since this is a small poc for a coding challenge the architecture is reduced to only splitting the data, repository and app modules.

### app

Contains the applications UI and UX implementation. Catches user interactions and forwards these to the according implementations to be handled.
Implements the presentation module to access the applications business logic in form of a ViewModel.
Directly communicates with the repository to trigger loading of data, via ViewModel(s).

### repository

Contains means to interact with local and external data sources. Requests, sends and stores data.
Implements the data module to access the applications entitites.

### data

Contains the applications entities, defining the models returned from and sent to the backend.


## Code Documentation
The folder documentation contains a HTML based code documentation created facilitating the Dokka framework.
To view it, it needs to be opened locally inside a browser of the users choice.

Code documentation is created by running the following task in the terminal. The result is stored inside the **documentation** folder under **Dokka**


## Code Quality
The codes quality is checked by facilitating the Detekt.
Detekt is used to find code smells, analyse code complexity with a configurable rule set.

### Run tasks
Detekt is run by calling the following gradle task in the terminal. The result is stored inside the **documentation** folder under **Detekt**

**./gradlew detektAll**


## Permissions
| Name | Handle | Use Case | User Consent Required? |
|---|---|---|---|
| Internet | android.permission.INTERNET | Communicating with external resources | No |

## Dependencies
| Name | Version | Use Case | Link |
|---|---|---|---|
| Kotlin Gradle Plugin | 1.6.10 | Plugin to build apps written in Kotlin | https://mvnrepository.com/artifact/org.jetbrains.kotlin/kotlin-gradle-plugin |
| Kotlin Standard Lib | 1.6.10 | Kotlin Language Library | https://mvnrepository.com/artifact/org.jetbrains.kotlin/kotlin-stdlib |
| Kotlin Reflect | 1.6.10 | Kotlin Reflection Library | https://mvnrepository.com/artifact/org.jetbrains.kotlin/kotlin-reflect |
| Kotlin Coroutines Core | 1.5.2 | Kotlin Coroutines Library | https://mvnrepository.com/artifact/org.jetbrains.kotlinx/kotlinx-coroutines-core |
| Kotlin Coroutines Android | 1.5.2 | Kotlin Coroutines Android Library | https://mvnrepository.com/artifact/org.jetbrains.kotlinx/kotlinx-coroutines-android |
| Kotlin Serialization JSON | 1.0.0 | Kotlin JSON Serialization Library | https://mvnrepository.com/artifact/org.jetbrains.kotlinx/kotlinx-serialization-json |
| Android Gradle Tools | 7.0.4 | Plugin to build Android apps | https://mvnrepository.com/artifact/com.android.tools.build/gradle |
| AndroidX App Compat | 1.4.1 | AndroidX base components | https://mvnrepository.com/artifact/androidx.appcompat/appcompat |
| AndroidX KTX Core | 1.7.0 | AndroidX Kotlin Components | https://mvnrepository.com/artifact/androidx.core/core-ktx |
| AndroidX Constraint Layout | 2.1.3 | Constraint Layout (Extended layouting functionalities) | https://mvnrepository.com/artifact/androidx.constraintlayout/constraintlayout |
| AndroidX Recycler View | 1.2.1 | Recycler View (Recycling list views) | https://mvnrepository.com/artifact/androidx.recyclerview/recyclerview |
| AndroidX Lifecycle Runtime | 2.4.0 | Lifecycle app base components | https://mvnrepository.com/artifact/androidx.lifecycle/lifecycle-runtime-ktx |
| AndroidX Lifecycle Common Java8 | 2.4.0 | Lifecycle Java8 components | https://mvnrepository.com/artifact/androidx.lifecycle/lifecycle-common-java8 |
| AndroidX Lifecycle Viewmodel | 2.4.0 | Viewmodel Components | https://mvnrepository.com/artifact/androidx.lifecycle/lifecycle-viewmodel-ktx |
| AndroidX Lifecycle Livedata KTX | 2.4.0 | Lifecycle Livedata Kotlin components | https://mvnrepository.com/artifact/androidx.lifecycle/lifecycle-livedata-ktx |
| AndroidX Navigation UI | 2.3.5 | Navigation framework | https://mvnrepository.com/artifact/androidx.navigation/navigation-ui-ktx |
| AndroidX Navigation Fragment | 2.3.5 | Navigation fragment components | https://mvnrepository.com/artifact/androidx.navigation/navigation-fragment-ktx |
| AndroidX Navigation SafeArgs | 2.3.5 | Navigation safeargs | https://mvnrepository.com/artifact/androidx.navigation/navigation-safe-args-gradle-plugin?repo=google |
| AndroidX Paging Runtime | 3.1.0 | Recycler View Paging | https://mvnrepository.com/artifact/androidx.paging/paging-runtime |
| GSON | 2.8.9 | Google Json Serialization | https://mvnrepository.com/artifact/com.google.code.gson/gson |
| KOIN Android | 2.1.6 | Kotlin dependency injection for Android | https://mvnrepository.com/artifact/org.koin/koin-android |
| KOIN ViewModel | 2.1.6 | Kotlin dependency injection viewmodel components | https://mvnrepository.com/artifact/org.koin/koin-androidx-viewmodel |
| Material Design | 1.5.0 | Android Material Design components | https://mvnrepository.com/artifact/com.google.android.material/material |
| Okio | 2.9.0 | Base Networking Framework | https://mvnrepository.com/artifact/com.squareup.okio/okio |
| OkHttp | 4.9.0 | HTTP/S network components | https://mvnrepository.com/artifact/com.squareup.okhttp3/okhttp |
| OkHttp Logging-Interceptor | 4.9.0 | Logging of network requests | https://mvnrepository.com/artifact/com.squareup.okhttp3/logging-interceptor |
| Retrofit | 2.9.0 | Extended network capabilities | https://mvnrepository.com/artifact/com.squareup.retrofit2/retrofit |
| Retrofit Kotlin Serialization Adapter | 0.8.0 | Parsing network data to model classes with Kotlin serialization | https://mvnrepository.com/artifact/com.jakewharton.retrofit/retrofit2-kotlinx-serialization-converter |
| Retrofit Coroutines Adapter | 0.9.2 | Coroutine Support for Retrofit | https://mvnrepository.com/artifact/com.jakewharton.retrofit/retrofit2-kotlin-coroutines-adapter |
| Timber | 5.0.1 | Advanced logging | https://mvnrepository.com/artifact/com.jakewharton.timber/timber |


### Code Quality and Documentation
| Name | Version | Use Case | Link |
|---|---|---|---|
| Detekt | 1.19.0 | Static Kotlin Code Analysis | https://mvnrepository.com/artifact/io.gitlab.arturbosch.detekt/detekt-api |
| Dokka | 1.6.10 | Code Documentation generator for Kotlin | https://mvnrepository.com/artifact/org.jetbrains.dokka/dokka-gradle-plugin |

package com.pkoerfer.data.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * Model class representing details for a movie
 */
@Serializable
data class MovieDetails(val id: Int? = 0,
                        val adult: Boolean = false,
                        val budget: Int? = 0,
                        val genres: List<Genre>? = listOf(),
                        val homepage: String? = "",
                        val overview: String? = "",
                        val popularity: Double? = 0.0,
                        val revenue: Int? = 0,
                        val runtime: Int? = 0,
                        val status: String? = "",
                        val tagline: String? = "",
                        val title: String? = "",
                        val video: Boolean? = false,
                        @SerialName("backdrop_path") val backdropPath: String? = "",
                        @SerialName("belongs_to_collection") val belongsToCollection: MovieCollection? = MovieCollection(),
                        @SerialName("imdb_id") val imdbId: String? = "",
                        @SerialName("original_language") val originalLanguage: String? = "",
                        @SerialName("original_title") val originalTitle: String? = "",
                        @SerialName("poster_path") val posterPath: String? = "",
                        @SerialName("production_companies") val productionCompanies: List<ProductionCompany>? = listOf(),
                        @SerialName("production_countries") val productionCountries: List<ProductionCountry>? = listOf(),
                        @SerialName("release_date") val releaseDate: String? = "",
                        @SerialName("spoken_languages") val spokenLanguages: List<SpokenLanguage>? = listOf(),
                        @SerialName("vote_average") val voteAverage: Double? = 0.0,
                        @SerialName("vote_count") val voteCount: Int? = 0) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MovieDetails

        if (id != other.id) return false
        if (adult != other.adult) return false
        if (budget != other.budget) return false
        if (genres != other.genres) return false
        if (homepage != other.homepage) return false
        if (overview != other.overview) return false
        if (popularity != other.popularity) return false
        if (revenue != other.revenue) return false
        if (runtime != other.runtime) return false
        if (status != other.status) return false
        if (tagline != other.tagline) return false
        if (title != other.title) return false
        if (video != other.video) return false
        if (backdropPath != other.backdropPath) return false
        if (belongsToCollection != other.belongsToCollection) return false
        if (imdbId != other.imdbId) return false
        if (originalLanguage != other.originalLanguage) return false
        if (originalTitle != other.originalTitle) return false
        if (posterPath != other.posterPath) return false
        if (productionCompanies != other.productionCompanies) return false
        if (productionCountries != other.productionCountries) return false
        if (releaseDate != other.releaseDate) return false
        if (spokenLanguages != other.spokenLanguages) return false
        if (voteAverage != other.voteAverage) return false
        if (voteCount != other.voteCount) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id ?: 0
        result = 31 * result + adult.hashCode()
        result = 31 * result + (budget ?: 0)
        result = 31 * result + (genres?.hashCode() ?: 0)
        result = 31 * result + (homepage?.hashCode() ?: 0)
        result = 31 * result + (overview?.hashCode() ?: 0)
        result = 31 * result + (popularity?.hashCode() ?: 0)
        result = 31 * result + (revenue ?: 0)
        result = 31 * result + (runtime ?: 0)
        result = 31 * result + (status?.hashCode() ?: 0)
        result = 31 * result + (tagline?.hashCode() ?: 0)
        result = 31 * result + (title?.hashCode() ?: 0)
        result = 31 * result + (video?.hashCode() ?: 0)
        result = 31 * result + (backdropPath?.hashCode() ?: 0)
        result = 31 * result + (belongsToCollection?.hashCode() ?: 0)
        result = 31 * result + (imdbId?.hashCode() ?: 0)
        result = 31 * result + (originalLanguage?.hashCode() ?: 0)
        result = 31 * result + (originalTitle?.hashCode() ?: 0)
        result = 31 * result + (posterPath?.hashCode() ?: 0)
        result = 31 * result + (productionCompanies?.hashCode() ?: 0)
        result = 31 * result + (productionCountries?.hashCode() ?: 0)
        result = 31 * result + (releaseDate?.hashCode() ?: 0)
        result = 31 * result + (spokenLanguages?.hashCode() ?: 0)
        result = 31 * result + (voteAverage?.hashCode() ?: 0)
        result = 31 * result + (voteCount ?: 0)
        return result
    }
}

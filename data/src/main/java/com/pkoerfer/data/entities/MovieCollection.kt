package com.pkoerfer.data.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * Model class representing a movie collection, e.g. franchise
 */
@Serializable
data class MovieCollection(val id: Int? = 0,
                           val name: String? = "",
                           @SerialName("poster_path") val posterPath: String? = "",
                           @SerialName("backdrop_path") val backdropPath: String? = "") {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MovieCollection

        if (id != other.id) return false
        if (name != other.name) return false
        if (posterPath != other.posterPath) return false
        if (backdropPath != other.backdropPath) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id ?: 0
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (posterPath?.hashCode() ?: 0)
        result = 31 * result + (backdropPath?.hashCode() ?: 0)
        return result
    }
}

package com.pkoerfer.data.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * Model class representing a production company
 */
@Serializable
data class ProductionCompany(val id: Int? = 0,
                             val name: String? = "",
                             @SerialName("logo_path") val logoPath: String? = "",
                             @SerialName("origin_country") val originCountry: String? = "") {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ProductionCompany

        if (id != other.id) return false
        if (name != other.name) return false
        if (logoPath != other.logoPath) return false
        if (originCountry != other.originCountry) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id ?: 0
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (logoPath?.hashCode() ?: 0)
        result = 31 * result + (originCountry?.hashCode() ?: 0)
        return result
    }
}

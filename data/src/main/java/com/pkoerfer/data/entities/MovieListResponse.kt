package com.pkoerfer.data.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * model class representing the reponse from calling
 *
 * https://api.themoviedb.org/3/movie/popular?api_key=KEY&page=1
 */
@Serializable
data class MovieListResponse(val page: Int = 0,
                             val results: List<Movie> = listOf(),
                             @SerialName("total_pages") val totalPages: Int = 0,
                             @SerialName("total_results") val totalResults: Int = 0) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MovieListResponse

        if (page != other.page) return false
        if (results != other.results) return false
        if (totalPages != other.totalPages) return false
        if (totalResults != other.totalResults) return false

        return true
    }

    override fun hashCode(): Int {
        var result = page
        result = 31 * result + results.hashCode()
        result = 31 * result + totalPages
        result = 31 * result + totalResults.hashCode()
        return result
    }
}

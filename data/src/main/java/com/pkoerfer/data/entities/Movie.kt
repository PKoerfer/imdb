package com.pkoerfer.data.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * model class representing a single movie
 */
@Serializable
data class Movie(val adult: Boolean? = false,
                 val id: Int? = 0,
                 val overview: String? = "",
                 val popularity: Double = 0.0,
                 val title: String? = "",
                 val video: Boolean? = false,
                 @SerialName("backdrop_path") val backdropPath: String? = "",
                 @SerialName("genre_ids") val genreIds: List<Int>? = listOf(),
                 @SerialName("original_language") val originalLanguage: String? = "",
                 @SerialName("original_title") val originalTitle: String? = "",
                 @SerialName("poster_path") val posterPath: String? = "",
                 @SerialName("release_date") val releaseDate: String? = "",
                 @SerialName("vote_average") val voteAverage: Double? = 0.0,
                 @SerialName("vote_count") val voteCount: Int? = 0) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Movie

        if (adult != other.adult) return false
        if (id != other.id) return false
        if (overview != other.overview) return false
        if (popularity != other.popularity) return false
        if (title != other.title) return false
        if (video != other.video) return false
        if (backdropPath != other.backdropPath) return false
        if (genreIds != other.genreIds) return false
        if (originalLanguage != other.originalLanguage) return false
        if (originalTitle != other.originalTitle) return false
        if (posterPath != other.posterPath) return false
        if (releaseDate != other.releaseDate) return false
        if (voteAverage != other.voteAverage) return false
        if (voteCount != other.voteCount) return false

        return true
    }

    override fun hashCode(): Int {
        var result = adult?.hashCode() ?: 0
        result = 31 * result + (id ?: 0)
        result = 31 * result + (overview?.hashCode() ?: 0)
        result = 31 * result + popularity.hashCode()
        result = 31 * result + (title?.hashCode() ?: 0)
        result = 31 * result + (video?.hashCode() ?: 0)
        result = 31 * result + (backdropPath?.hashCode() ?: 0)
        result = 31 * result + (genreIds?.hashCode() ?: 0)
        result = 31 * result + (originalLanguage?.hashCode() ?: 0)
        result = 31 * result + (originalTitle?.hashCode() ?: 0)
        result = 31 * result + (posterPath?.hashCode() ?: 0)
        result = 31 * result + (releaseDate?.hashCode() ?: 0)
        result = 31 * result + (voteAverage?.hashCode() ?: 0)
        result = 31 * result + (voteCount ?: 0)
        return result
    }
}

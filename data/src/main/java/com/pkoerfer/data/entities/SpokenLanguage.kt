package com.pkoerfer.data.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * Model class representing a spoken language
 */
@Serializable
data class SpokenLanguage(val name: String? = "",
                          @SerialName("english_name") val englishName: String? = "",
                          @SerialName("iso_639_1") val iso6391: String? = "") {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SpokenLanguage

        if (name != other.name) return false
        if (englishName != other.englishName) return false
        if (iso6391 != other.iso6391) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name?.hashCode() ?: 0
        result = 31 * result + (englishName?.hashCode() ?: 0)
        result = 31 * result + (iso6391?.hashCode() ?: 0)
        return result
    }
}

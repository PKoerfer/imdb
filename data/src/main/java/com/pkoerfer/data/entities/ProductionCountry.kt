package com.pkoerfer.data.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * Model class representing a production country
 */
@Serializable
data class ProductionCountry(@SerialName("iso_3166_1") val iso31661: String? = "", val name: String? = "") {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ProductionCountry

        if (iso31661 != other.iso31661) return false
        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        var result = iso31661?.hashCode() ?: 0
        result = 31 * result + name.hashCode()
        return result
    }
}

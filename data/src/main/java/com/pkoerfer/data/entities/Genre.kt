package com.pkoerfer.data.entities

import kotlinx.serialization.Serializable

/**
 * Model class representing a genre
 */
@Serializable
data class Genre(val id: Int? = 0, val name: String? = "") {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Genre

        if (id != other.id) return false
        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id ?: 0
        result = 31 * result + (name?.hashCode() ?: 0)
        return result
    }
}

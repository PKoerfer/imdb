package com.pkoerfer.data

/**
 * status of the resource, enabling handling of these in the UI
 */
enum class ResourceState {
    SUCCESS,
    ERROR
}

/**
 * LiveData wrapper used to inform the UI about request state changes
 */
data class Resource<out T>(val status: ResourceState, val data: T?, val error: Throwable?) {

    companion object {
        inline fun <reified T> from(data: Any?): Resource<T> {
            return when (data) {
                is Throwable -> Resource.error(data)
                null -> Resource.error(NullPointerException("data is null"))
                !is T -> {
                    Resource.error(
                        IllegalArgumentException("data is invalid type: was ${data::class.simpleName} but should be " + T::class.simpleName)
                    )
                }
                else -> Resource.success(data)
            }
        }

        fun <T> success(data: T): Resource<T> {
            return Resource(ResourceState.SUCCESS, data, null)
        }

        fun <T> error(throwable: Throwable): Resource<T> {
            return Resource(ResourceState.ERROR, null, throwable)
        }
    }
}

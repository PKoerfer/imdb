package com.pkoerfer.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.pkoerfer.data.Resource
import com.pkoerfer.data.entities.MovieDetails
import com.pkoerfer.data.entities.MovieListResponse

/**
 * Repository class used to wrap backend responses into the desired containers to enable detailed call status tracking
 */
class Repository(private val remote: Remote) {

    fun loadMovieDetails(movieId: Int): LiveData<Resource<MovieDetails>> {
        return liveData {
            try {
                emit(Resource.success(remote.loadMovieDetails(movieId)))
            } catch (ex: Throwable) {
                emit(Resource.error<MovieDetails>(ex))
            }
        }
    }

    fun searchMovies(query: String): LiveData<Resource<MovieListResponse>> {
        return liveData {
            try {
                emit(Resource.success(remote.searchMovies(query)))
            } catch (ex: Throwable) {
                emit(Resource.error<MovieListResponse>(ex))
            }
        }
    }
}

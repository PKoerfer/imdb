package com.pkoerfer.repository.pagingsources

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.pkoerfer.data.entities.Movie
import com.pkoerfer.repository.Remote

/**
 * PagingSource used to load most popular movies setup with automated pagination
 *
 * @param remote the remote implementation enabling access to the api
 */
class PopularMoviesPagingSource(private val remote: Remote): PagingSource<Int, Movie>() {

    override fun getRefreshKey(state: PagingState<Int, Movie>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1)?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Movie> {
        return try {
            val result = remote.loadPopularMovies(params.key?: DEFAULT_PAGE)
            LoadResult.Page(result.results, prevKey = null, nextKey = result.page + 1)
        } catch(exception: Exception) {
            return LoadResult.Error(exception)
        }
    }

    companion object {
        const val DEFAULT_PAGE = 1
        const val PAGE_SIZE = 20
    }
}

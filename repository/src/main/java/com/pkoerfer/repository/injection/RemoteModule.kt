package com.pkoerfer.repository.injection

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.pkoerfer.repository.services.MovieService
import com.pkoerfer.repository.Remote
import com.pkoerfer.repository.Repository
import com.pkoerfer.repository.TokenInterceptor
import com.pkoerfer.repository.services.SearchService
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

const val CALL_TIMEOUT = 15L
val JSON_MEDIA_TYPE = "application/json".toMediaType()

/**
 * KOIN definition of injectable components for the repository layer
 */
@ExperimentalSerializationApi
val remoteModule = module {
    single<OkHttpClient> {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .addInterceptor(TokenInterceptor())
            .callTimeout(CALL_TIMEOUT, TimeUnit.SECONDS)
            .build()
    }

    single<Retrofit> {
        Retrofit.Builder()
            .client(get())
            .baseUrl("https://api.themoviedb.org/3/")
            .addConverterFactory(
                Json {
                    ignoreUnknownKeys = true
                }.asConverterFactory(JSON_MEDIA_TYPE)
            )
            .build()
    }

    single<Remote> {
        val retrofit = get<Retrofit>()
        val movieService = retrofit.create(MovieService::class.java)
        val searchService = retrofit.create(SearchService::class.java)
        Remote(movieService, searchService)
    }

    single<Repository> {
        Repository(get())
    }
}

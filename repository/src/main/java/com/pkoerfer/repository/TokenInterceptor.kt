package com.pkoerfer.repository

import okhttp3.Interceptor
import okhttp3.Response

/**
 * interceptor used to always add the api key to requests made to the backend
 */
class TokenInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val originalHttpUrl = originalRequest.url

        val newUrl = originalHttpUrl.newBuilder()
            .addQueryParameter("api_key", "4fa606720e3fb707002544c42ee06754")
            .build()
        val newRequest = originalRequest.newBuilder()
            .url(newUrl)
            .build()
        return chain.proceed(newRequest)
    }
}

package com.pkoerfer.repository

import com.pkoerfer.data.entities.MovieDetails
import com.pkoerfer.data.entities.MovieListResponse
import com.pkoerfer.repository.services.MovieService
import com.pkoerfer.repository.services.SearchService
import kotlinx.coroutines.CompletableDeferred
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response

/**
 * Class used for communication with the backend to load data
 */
class Remote(private val movieService: MovieService, private val searchService: SearchService) {

    suspend fun loadPopularMovies(page: Int): MovieListResponse {
        val deferred = CompletableDeferred<MovieListResponse>()
        movieService.loadPopularMovies(page).enqueue(object: Callback<MovieListResponse> {
            override fun onResponse(call: Call<MovieListResponse>, response: Response<MovieListResponse>) {
                response.body()?.let {
                    deferred.complete(it)
                }?: deferred.completeExceptionally(HttpException(response))
            }

            override fun onFailure(call: Call<MovieListResponse>, t: Throwable) {
                deferred.completeExceptionally(t)
            }
        })
        return deferred.await()
    }

    suspend fun loadMovieDetails(movieId: Int): MovieDetails {
        val deferred = CompletableDeferred<MovieDetails>()
        movieService.loadMovieDetails(movieId).enqueue(object: Callback<MovieDetails> {
            override fun onResponse(call: Call<MovieDetails>, response: Response<MovieDetails>) {
                response.body()?.let {
                    deferred.complete(it)
                }?: deferred.completeExceptionally(HttpException(response))
            }

            override fun onFailure(call: Call<MovieDetails>, t: Throwable) {
                deferred.completeExceptionally(t)
            }
        })
        return deferred.await()
    }

    suspend fun searchMovies(query: String): MovieListResponse {
        val deferred = CompletableDeferred<MovieListResponse>()
        searchService.searchMovie(query).enqueue(object: Callback<MovieListResponse> {
            override fun onResponse(call: Call<MovieListResponse>, response: Response<MovieListResponse>) {
                response.body()?.let {
                    deferred.complete(it)
                }?: deferred.completeExceptionally(HttpException(response))
            }

            override fun onFailure(call: Call<MovieListResponse>, t: Throwable) {
                deferred.completeExceptionally(t)
            }
        })
        return deferred.await()
    }
}

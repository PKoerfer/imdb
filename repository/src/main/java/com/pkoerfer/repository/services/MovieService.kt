package com.pkoerfer.repository.services

import com.pkoerfer.data.entities.MovieDetails
import com.pkoerfer.data.entities.MovieListResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Retrofit service defining calls made to the movie backend
 */
interface MovieService {

    @GET("movie/popular")
    fun loadPopularMovies(@Query("page") page: Int): Call<MovieListResponse>

    @GET("movie/{movieId}")
    fun loadMovieDetails(@Path("movieId") movieId: Int): Call<MovieDetails>
}

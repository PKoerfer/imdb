package com.pkoerfer.repository.services

import com.pkoerfer.data.entities.MovieListResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Retrofit service defining calls made to the search api
 */
interface SearchService {

    @GET("search/movie")
    fun searchMovie(@Query("query") query: String): Call<MovieListResponse>
}

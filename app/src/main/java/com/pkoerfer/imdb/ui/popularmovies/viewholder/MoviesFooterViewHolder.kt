package com.pkoerfer.imdb.ui.popularmovies.viewholder

import android.view.View
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.pkoerfer.imdb.R
import com.pkoerfer.imdb.databinding.ViewholderMovieLoadstateBinding
import com.pkoerfer.imdb.ui.Constants
import retrofit2.HttpException

/**
 * ViewHolder displaying error states
 */
class MoviesFooterViewHolder(view: View): RecyclerView.ViewHolder(view) {

    fun bind(loadState: LoadState) {
        if (loadState is LoadState.Error) {
            val error = loadState.error
            if (error is HttpException) {
                val errorResId = when (error.code()) {
                    Constants.RESPONSE_CODE_PAGINATION_END -> {
                        R.string.movies_loadState_endReached
                    }
                    else -> {
                        R.string.movies_loadState_generalError
                    }
                }
                ViewholderMovieLoadstateBinding.bind(itemView).apply {
                    loadStateTitle.setText(errorResId)
                }
            }
        }
    }
}

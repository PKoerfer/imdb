package com.pkoerfer.imdb.ui.popularmovies.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import com.pkoerfer.imdb.R
import com.pkoerfer.imdb.ui.popularmovies.viewholder.MoviesFooterViewHolder

/**
 * Adapter used to display when pagination has reached the end
 *
 */
class MovieLoadStateAdapter: LoadStateAdapter<MoviesFooterViewHolder>() {

    override fun onBindViewHolder(holder: MoviesFooterViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): MoviesFooterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.viewholder_movie_loadstate, parent, false)
        return MoviesFooterViewHolder(view)
    }
}

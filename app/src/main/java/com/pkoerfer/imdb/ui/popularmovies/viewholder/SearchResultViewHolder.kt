package com.pkoerfer.imdb.ui.popularmovies.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.pkoerfer.data.entities.Movie
import com.pkoerfer.imdb.base.recyclerview.IdClickListener
import com.pkoerfer.imdb.databinding.ViewholderSearchResultBinding

/**
 * ViewHolder used to display a movie returned by the search
 */
class SearchResultViewHolder(view: View, private val clickListener: IdClickListener): RecyclerView.ViewHolder(view) {

    fun bind(movie: Movie) {
        ViewholderSearchResultBinding.bind(itemView).apply {
            root.setOnClickListener {
                clickListener.onItemClicked(movie.id?: 0)
            }
            searchResultName.text = movie.title?: ""
        }
    }
}

package com.pkoerfer.imdb.ui.popularmovies.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.pkoerfer.data.entities.Movie
import com.pkoerfer.imdb.R
import com.pkoerfer.imdb.base.recyclerview.IdClickListener
import com.pkoerfer.imdb.base.toIntPercentage
import com.pkoerfer.imdb.databinding.ViewholderMovieBinding
import com.pkoerfer.imdb.ui.Constants

/**
 * ViewHolder used to display popular movie data
 */
class MovieViewHolder(view: View, private val idClickListener: IdClickListener) :
    RecyclerView.ViewHolder(view) {

    fun bind(movie: Movie?) {
        ViewholderMovieBinding.bind(itemView).apply {
            movie?.let { m ->
                val rating = m.voteAverage?.toIntPercentage()?: 0
                val ratingBackgroundRes = when {
                    rating >= Constants.RATING_CAP_GOOD -> {
                        R.drawable.bg_good_rating
                    }
                    rating >= Constants.RATING_CAP_AVERAGE -> {
                        R.drawable.bg_average_rating
                    }
                    else -> {
                        R.drawable.bg_bad_rating
                    }
                }
                itemView.setOnClickListener {
                    idClickListener.onItemClicked(m.id?: FALLBACK_ID)
                }
                movieTitle.text = m.title
                movieRatingAverage.text = rating.toString()
                movieRatingAverage.setBackgroundResource(ratingBackgroundRes)
                m.backdropPath?.let { backdrop ->
                    val url = Constants.IMAGE_URL_PREFIX.format(backdrop)
                    Glide.with(movieImage)
                        .load(url)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .optionalCenterCrop()
                        .into(movieImage)
                }
            }
        }
    }

    companion object {
        private const val FALLBACK_ID = 0
    }
}

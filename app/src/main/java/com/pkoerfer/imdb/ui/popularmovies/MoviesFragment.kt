package com.pkoerfer.imdb.ui.popularmovies

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.pkoerfer.data.ResourceState
import com.pkoerfer.imdb.R
import com.pkoerfer.imdb.base.recyclerview.IdClickListener
import com.pkoerfer.imdb.databinding.FragmentMoviesBinding
import com.pkoerfer.imdb.ui.Constants
import com.pkoerfer.imdb.ui.popularmovies.adapter.MovieAdapter
import com.pkoerfer.imdb.ui.popularmovies.adapter.MovieDiffUtil
import com.pkoerfer.imdb.ui.popularmovies.adapter.MovieLoadStateAdapter
import com.pkoerfer.imdb.ui.popularmovies.adapter.SearchDiffUtilCallback
import com.pkoerfer.imdb.ui.popularmovies.adapter.SearchResultsAdapter
import com.pkoerfer.imdb.viewmodel.MovieViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

/**
 * fragment used to display popular movies
 */
class MoviesFragment: Fragment() {

    private val movieViewModel: MovieViewModel by sharedViewModel()

    private var binding: FragmentMoviesBinding? = null
    private var searchView: SearchView? = null
    private var searchQuery: String = ""

    private lateinit var movieAdapter: MovieAdapter
    private lateinit var searchResultsAdapter: SearchResultsAdapter

    private val movieClickListener: IdClickListener = object: IdClickListener {
        override fun onItemClicked(id: Int) {
            findNavController().navigate(
                MoviesFragmentDirections.actionMoviesFragmentToMovieDetailFragment(id)
            )
        }
    }

    private val queryListener: SearchView.OnQueryTextListener = object: SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            // do nothing since search is triggered when text has changed (see below)
            return false
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            newText?.let { query ->
                if (query.length >= Constants.SEARCH_MIN_LENGTH) {
                    searchQuery = query
                    movieViewModel.searchMovie(query).observe(viewLifecycleOwner, { resource ->
                        when (resource.status) {
                            ResourceState.SUCCESS -> {
                                resource.data?.let { movies ->
                                    binding?.apply {
                                        moviesSearchContainer.visibility = View.VISIBLE
                                        searchResultsAdapter.update(movies.results)
                                    }
                                }
                            }
                            ResourceState.ERROR -> {
                                // display error
                            }
                        }
                    })
                } else if (query.isEmpty()) {
                    searchQuery = query
                    binding?.apply {
                        moviesSearchContainer.visibility = View.GONE
                        searchResultsAdapter.update(listOf())
                    }
                }
            }
            return true
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movies, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        movieAdapter = MovieAdapter(MovieDiffUtil(), movieClickListener)
        searchResultsAdapter = SearchResultsAdapter(SearchDiffUtilCallback(), movieClickListener)

        binding = FragmentMoviesBinding.bind(view).apply {
            moviesRecyclerView.adapter = movieAdapter
                .withLoadStateFooter(MovieLoadStateAdapter())
            moviesRecyclerView.layoutManager = LinearLayoutManager(view.context)
            (requireActivity() as AppCompatActivity).setSupportActionBar(moviesToolbar)
            moviesSearchResults.layoutManager = LinearLayoutManager(view.context)
            moviesSearchResults.adapter = searchResultsAdapter
        }
    }

    override fun onResume() {
        super.onResume()
        movieViewModel.popularMovies.observe(viewLifecycleOwner, { pagingData ->
            movieAdapter.submitData(viewLifecycleOwner.lifecycle, pagingData)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_search, menu)
        val searchItem = menu.findItem(R.id.movie_search)
        val manager = requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager

        if (searchItem != null) {
            searchView = searchItem.actionView as SearchView
        }
        searchView?.apply {
            setSearchableInfo(manager.getSearchableInfo(activity?.componentName))
            setOnQueryTextListener(queryListener)
            if (searchQuery.isNotEmpty()) {
                setQuery(searchQuery, true)
                isIconified = false
                queryHint = getString(R.string.movieSearch_hint)
            }
        }
        super.onCreateOptionsMenu(menu, inflater)
    }
}

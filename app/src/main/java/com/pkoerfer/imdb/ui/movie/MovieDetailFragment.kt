package com.pkoerfer.imdb.ui.movie

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.pkoerfer.data.Resource
import com.pkoerfer.data.ResourceState
import com.pkoerfer.data.entities.MovieDetails
import com.pkoerfer.imdb.R
import com.pkoerfer.imdb.base.toIntPercentage
import com.pkoerfer.imdb.databinding.FragmentMovieDetailBinding
import com.pkoerfer.imdb.ui.Constants
import com.pkoerfer.imdb.ui.popularmovies.viewholder.MovieViewHolder
import com.pkoerfer.imdb.viewmodel.MovieViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

/**
 * Fragment used to display detail information of an movie
 */
class MovieDetailFragment: Fragment() {

    private val movieViewModel: MovieViewModel by sharedViewModel()

    private var binding: FragmentMovieDetailBinding? = null

    private val movieDetailsObserver: Observer<Resource<MovieDetails>> = Observer { resource ->
        when (resource.status) {
            ResourceState.SUCCESS -> {
                display()
            }
            ResourceState.ERROR -> {
                binding?.apply {
                    movieDetailsOverview.key.setText(R.string.movies_loadState_generalError_header)
                    movieDetailsOverview.value.setText(R.string.movies_loadState_generalError)
                }
            }
        }
    }

    private fun display() {
        movieViewModel.movieDetails.value?.data?.let { movieDetails ->
            binding?.apply {
                val rating = movieDetails.voteAverage?.toIntPercentage()?: 0
                val ratingBackgroundRes = when {
                    rating >= Constants.RATING_CAP_GOOD -> {
                        R.drawable.bg_good_rating
                    }
                    rating >= Constants.RATING_CAP_AVERAGE -> {
                        R.drawable.bg_average_rating
                    }
                    else -> {
                        R.drawable.bg_bad_rating
                    }
                }
                movieDetailsTitle.text = movieDetails.title?: ""
                movieDetailsSubTitle.text = movieDetails.tagline?: ""
                movieDetailsAverageRating.text = movieDetails.voteAverage?.toIntPercentage()?.toString()?: "0"
                movieDetailsAverageRating.setBackgroundResource(ratingBackgroundRes)

                movieDetailsOverview.root.isVisible = movieDetails.overview?.isNotEmpty()?: false
                movieDetailsOverview.key.setText(R.string.movieDetail_overview_key)
                movieDetailsOverview.value.text = movieDetails.overview?: ""
                movieDetailsRelease.root.isVisible = movieDetails.releaseDate?.isNotEmpty()?: false
                movieDetailsRelease.key.setText(R.string.movieDetail_releaseDate_key)
                movieDetailsRelease.value.text = movieDetails.releaseDate?: ""
                movieDetailsRuntime.root.isVisible = movieDetails.runtime != null
                movieDetailsRuntime.key.setText(R.string.movieDetail_runtime_key)
                movieDetailsRuntime.value.text = getString(R.string.movieDetail_runtime_value, movieDetails.runtime?: 0)
                movieDetailsCollection.root.isVisible = movieDetails.belongsToCollection != null
                movieDetailsCollection.key.setText(R.string.movieDetail_collection_key)
                movieDetailsCollection.value.text = movieDetails.belongsToCollection?.name?: ""
                movieDetailsGenre.root.isVisible = movieDetails.genres?.isNotEmpty()?: false
                movieDetailsGenre.key.setText(R.string.movieDetail_genre_key)
                movieDetailsGenre.value.text = movieDetails.genres?.joinToString {
                    it.name?: ""
                }
                movieDetailsCompanies.root.isVisible = movieDetails.productionCompanies?.isNotEmpty()?: false
                movieDetailsCompanies.key.setText(R.string.movieDetail_companies_key)
                movieDetailsCompanies.value.text = movieDetails.productionCompanies?.joinToString {
                    it.name?: ""
                }
                movieDetailsCountries.root.isVisible = movieDetails.productionCountries?.isNotEmpty()?: false
                movieDetailsCountries.key.setText(R.string.movieDetail_countries_key)
                movieDetailsCountries.value.text = movieDetails.productionCountries?.joinToString {
                    it.name?: ""
                }
                movieDetailsLanguages.root.isVisible = movieDetails.spokenLanguages?.isNotEmpty()?: false
                movieDetailsLanguages.key.setText(R.string.movieDetail_languages_key)
                movieDetailsLanguages.value.text = movieDetails.spokenLanguages?.joinToString {
                    if (it.name != null) {
                        it.name!!
                    } else {
                        it.englishName?: ""
                    }
                }
                movieDetails.backdropPath?.let { backdrop ->
                    val url = Constants.IMAGE_URL_PREFIX.format(backdrop)
                    Glide.with(movieDetailsImage)
                        .load(url)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .optionalCenterCrop()
                        .into(movieDetailsImage)
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movie_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMovieDetailBinding.bind(view)
    }

    override fun onResume() {
        super.onResume()
        val id = navArgs<MovieDetailFragmentArgs>().value.movieId
        if (id != movieViewModel.movieDetails.value?.data?.id) {
            movieViewModel.loadMovieDetails(id)
            movieViewModel.movieDetails.observe(viewLifecycleOwner, movieDetailsObserver)
        } else {
            display()
        }
    }
}

package com.pkoerfer.imdb.ui.popularmovies.adapter

import androidx.recyclerview.widget.DiffUtil
import com.pkoerfer.data.entities.Movie

/**
 * Diff util callback to prevent unnecessary initialisation of views
 */
class SearchDiffUtilCallback: DiffUtil.Callback() {

    private lateinit var oldList: List<Movie>
    private lateinit var newList: List<Movie>

    fun update(oldItems: List<Movie>, newItems: List<Movie>) {
        oldList = oldItems
        newList = newItems
    }

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }
}

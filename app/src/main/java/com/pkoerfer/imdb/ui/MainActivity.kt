package com.pkoerfer.imdb.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.pkoerfer.imdb.R
import com.pkoerfer.imdb.viewmodel.MovieViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Activity class used to create viewmodel(s) and navigation
 */
class MainActivity : AppCompatActivity() {

    private val movieViewModel: MovieViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}

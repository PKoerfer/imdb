package com.pkoerfer.imdb.ui.popularmovies.adapter

import androidx.recyclerview.widget.DiffUtil
import com.pkoerfer.data.entities.Movie

/**
 * Util class used to created aff of changes between old adapter data and new adapter data
 * for a recyclerview displaying movies
 */
class MovieDiffUtil: DiffUtil.ItemCallback<Movie>() {

    override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem == newItem
    }
}

package com.pkoerfer.imdb.ui

/**
 * object containing constants needed throughout the app
 */
object Constants {
    const val IMAGE_URL_PREFIX = "https://www.themoviedb.org/t/p/w1280/%s"
    const val RESPONSE_CODE_PAGINATION_END = 422
    const val SEARCH_MIN_LENGTH = 3
    const val RATING_CAP_GOOD = 70
    const val RATING_CAP_AVERAGE = 50

    const val PERCENTAGE_BASE = 10
}

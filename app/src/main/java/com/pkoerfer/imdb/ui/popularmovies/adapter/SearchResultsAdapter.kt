package com.pkoerfer.imdb.ui.popularmovies.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.pkoerfer.data.entities.Movie
import com.pkoerfer.imdb.R
import com.pkoerfer.imdb.base.recyclerview.IdClickListener
import com.pkoerfer.imdb.ui.popularmovies.viewholder.SearchResultViewHolder

/**
 * Adapter used to display search movie data
 */
class SearchResultsAdapter(private val diffUtilCallback: SearchDiffUtilCallback,
                           private val clickListener: IdClickListener): RecyclerView.Adapter<SearchResultViewHolder>() {

    private var movies: List<Movie> = listOf()

    fun update(items: List<Movie>) {
        diffUtilCallback.update(movies, items)
        val diff = DiffUtil.calculateDiff(diffUtilCallback, true)
        movies = items
        diff.dispatchUpdatesTo(this@SearchResultsAdapter)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchResultViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.viewholder_search_result, parent, false)
        return SearchResultViewHolder(view, clickListener)
    }

    override fun onBindViewHolder(holder: SearchResultViewHolder, position: Int) {
        holder.bind(movies[position])
    }

    override fun getItemCount(): Int {
        return movies.size
    }
}

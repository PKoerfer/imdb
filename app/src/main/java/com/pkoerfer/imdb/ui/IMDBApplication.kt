package com.pkoerfer.imdb.ui

import android.app.Application
import com.pkoerfer.imdb.BuildConfig
import com.pkoerfer.imdb.injection.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

/**
 * Application class used to initialize components needed all around the app
 */
@kotlinx.serialization.InternalSerializationApi
@kotlinx.serialization.ExperimentalSerializationApi
class IMDBApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@IMDBApplication)
            modules(appModule)
        }
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}

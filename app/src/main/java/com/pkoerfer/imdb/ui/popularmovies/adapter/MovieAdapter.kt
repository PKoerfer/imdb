package com.pkoerfer.imdb.ui.popularmovies.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.pkoerfer.data.entities.Movie
import com.pkoerfer.imdb.R
import com.pkoerfer.imdb.base.recyclerview.IdClickListener
import com.pkoerfer.imdb.ui.popularmovies.viewholder.MovieViewHolder

/**
 * Adapter used to display paginated movie data
 * Pagination is handled by the base class PagingDataAdapter
 *
 * @param callback DiffUtil.ItemCallback used to enable the recyclerview
 * to only update items which where changed
 * @param idClickListener clickListener added from fragment to handle click event on viewHolder
 */
class MovieAdapter(callback: DiffUtil.ItemCallback<Movie>, private val idClickListener: IdClickListener)
    : PagingDataAdapter<Movie, MovieViewHolder>(callback) {

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.viewholder_movie, parent, false)
        return MovieViewHolder(view, idClickListener)
    }
}

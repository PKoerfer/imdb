package com.pkoerfer.imdb.injection

import com.pkoerfer.imdb.viewmodel.MovieViewModel
import com.pkoerfer.repository.injection.remoteModule
import org.koin.dsl.module

/**
 * KOIN definition of injectable components for the application
 */
@kotlinx.serialization.InternalSerializationApi
@kotlinx.serialization.ExperimentalSerializationApi
val applicationModule = module {

    single {
        MovieViewModel(get(), get())
    }
}

/**
 * KOIN modules used in the Application class for initialisation
 */
@kotlinx.serialization.InternalSerializationApi
@kotlinx.serialization.ExperimentalSerializationApi
val appModule = listOf(applicationModule, remoteModule)

package com.pkoerfer.imdb.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import androidx.paging.liveData
import com.pkoerfer.data.Resource
import com.pkoerfer.data.entities.MovieDetails
import com.pkoerfer.data.entities.MovieListResponse
import com.pkoerfer.repository.Remote
import com.pkoerfer.repository.Repository
import com.pkoerfer.repository.pagingsources.PopularMoviesPagingSource

/**
 * ViewModel used to communicate with the repository layer to trigger loading data
 *
 * Fragments will observe on LiveData elements to get the results of loading data
 */
class MovieViewModel(private val remote: Remote, private val repository: Repository): ViewModel() {

    var movieDetails: LiveData<Resource<MovieDetails>> = MutableLiveData()

    val popularMovies = Pager(
        PagingConfig(PopularMoviesPagingSource.PAGE_SIZE), PopularMoviesPagingSource.DEFAULT_PAGE, {
            PopularMoviesPagingSource(remote)
        }).liveData.cachedIn(viewModelScope)

    fun loadMovieDetails(movieId: Int) {
        movieDetails = repository.loadMovieDetails(movieId)
    }

    fun searchMovie(query: String): LiveData<Resource<MovieListResponse>> {
        return repository.searchMovies(query)
    }
}

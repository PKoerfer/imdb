package com.pkoerfer.imdb.base

import com.pkoerfer.imdb.ui.Constants
import kotlin.math.roundToInt

/**
 * convenience class to format double values to percentages
 */
fun Double.toIntPercentage(): Int {
    return (this * Constants.PERCENTAGE_BASE).roundToInt()
}

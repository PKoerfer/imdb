package com.pkoerfer.imdb.base.recyclerview

/**
 * click listener used in ViewHolders
 */
interface IdClickListener {
    fun onItemClicked(id: Int)
}
